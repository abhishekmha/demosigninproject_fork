package com.abhi.dao;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginTestCase {

	@Test
	public void test() {
		LoginDao dao = new LoginDao();
		boolean result = dao.validate("Abhishek", "abhi");
		assertEquals(true, result);
	}

}
